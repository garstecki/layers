import com.neovisionaries.i18n.CountryCode
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import ui.AddressRequest
import ui.UserController
import ui.UserRequest
import java.util.*

internal class UserTest {
    private val controller = UserController()

    @Test
    fun `both methods returns the same response`() {
        val userRequest = createUserRequest()

        val response1 = controller.handleThroughoutAllLayers(userRequest)
        val response2 = controller.handleSkippingLayers(userRequest)

        Assertions.assertEquals(response1, response2)
    }

    @ParameterizedTest
    @ValueSource(ints = [1_000, 10_000])
    fun `execution time of all layers`(batch: Int) {
        val userRequests = Array(batch) { createUserRequest() }

        val start = System.currentTimeMillis()

        userRequests.forEach { controller.handleThroughoutAllLayers(it) }

        val end = System.currentTimeMillis()

        println("All layers execution of $batch - ${end - start}ms")
    }

    @ParameterizedTest
    @ValueSource(ints = [1_000, 10_000])
    fun `execution time of skipped layers`(batch: Int) {
        val userRequests = Array(batch) { createUserRequest() }

        val start = System.currentTimeMillis()

        userRequests.forEach { controller.handleSkippingLayers(it) }

        val end = System.currentTimeMillis()

        println("Skipped layers execution of $batch - ${end - start}ms")
    }

    private fun createUserRequest() = UserRequest(
        UUID.randomUUID(),
        "Damian",
        "Garstecki",
        Date(),
        "awsome@dev",
        AddressRequest("Some Street", "00-000", "Gdynia", CountryCode.PL)
    )
}
