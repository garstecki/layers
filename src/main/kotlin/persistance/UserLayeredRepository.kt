package persistance

import domain.User
import java.util.UUID

internal object UserLayeredRepository {
    private val inMemory = mutableSetOf<UserDao>()

    fun save(user: User) {
        inMemory.add(UserDao.fromEntity(user))
    }

    fun findById(id: UUID): User? = inMemory.find { id == it.id }?.toEntity()
}

internal object UserDirectRepository {
    private val inMemory = mutableSetOf<UserDao>()

    fun save(user: UserDao) {
        inMemory.add(user)
    }

    fun findById(id: UUID): UserDao? = inMemory.find { id == it.id }
}
