package persistance

import com.neovisionaries.i18n.CountryCode
import domain.Address
import domain.User
import java.util.Date
import java.util.UUID

internal data class UserDao(
    val id: UUID,
    val name: String,
    val lastName: String,
    val birth: Date,
    val email: String,
    val address: AddressDao
) {
    companion object {
        fun fromEntity(user: User) = UserDao(
            user.id,
            user.name,
            user.lastName,
            user.birth,
            user.email,
            AddressDao.fromEntity(user.address)
        )
    }

    fun toEntity(): User = User(id, name, lastName, birth, email, address.toEntity())
}

internal data class AddressDao(
    val street: String,
    val postCode: String,
    val city: String,
    val country: CountryCode
) {
    companion object {
        fun fromEntity(address: Address) = AddressDao(address.street, address.postCode, address.city, address.country)
    }

    fun toEntity(): Address = Address(street, postCode, city, country)
}
