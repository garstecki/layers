package ui

import application.AddressDto
import application.UserDto
import com.neovisionaries.i18n.CountryCode
import persistance.AddressDao
import persistance.UserDao
import java.util.Date
import java.util.UUID

internal data class UserRequest(
    val id: UUID,
    val name: String,
    val lastName: String,
    val birth: Date,
    val email: String,
    val address: AddressRequest
) {
    fun toDto() = UserDto(id, name, lastName, birth, email, address.toDto())

    fun toDao() = UserDao(id, name, lastName, birth, email, address.toDao())
}

internal data class AddressRequest(
    val street: String,
    val postCode: String,
    val city: String,
    val country: CountryCode
) {
    fun toDto() = AddressDto(street, postCode, city, country)

    fun toDao() = AddressDao(street, postCode, city, country)
}
