package ui

import application.CreateUserCommandHandler
import application.GetUserQueryHandler
import persistance.UserDirectRepository

internal class UserController {
    private val commandHandler = CreateUserCommandHandler()
    private val queryHandler = GetUserQueryHandler()

    fun handleThroughoutAllLayers(userRequest: UserRequest): UserResponse {
        commandHandler.handle(userRequest.toDto())

        return UserResponse.fromDto(queryHandler.handle(userRequest.id)!!)
    }

    private val repository = UserDirectRepository

    fun handleSkippingLayers(userRequest: UserRequest): UserResponse {
        repository.save(userRequest.toDao())

        return UserResponse.fromDao(repository.findById(userRequest.id)!!)
    }
}
