package ui

import application.AddressDto
import application.UserDto
import com.neovisionaries.i18n.CountryCode
import persistance.AddressDao
import persistance.UserDao
import java.util.Date
import java.util.UUID

internal data class UserResponse(
    val id: UUID,
    val name: String,
    val lastName: String,
    val birth: Date,
    val email: String,
    val address: AddressResponse
) {
    companion object {
        fun fromDto(user: UserDto) = UserResponse(
            user.id,
            user.name,
            user.lastName,
            user.birth,
            user.email,
            AddressResponse.fromDto(user.address)
        )

        fun fromDao(user: UserDao) = UserResponse(
            user.id,
            user.name,
            user.lastName,
            user.birth,
            user.email,
            AddressResponse.fromDao(user.address)
        )
    }
}

internal data class AddressResponse(
    val street: String,
    val postCode: String,
    val city: String,
    val country: CountryCode
) {
    companion object {
        fun fromDto(address: AddressDto) = AddressResponse(
            address.street,
            address.postCode,
            address.city,
            address.country
        )

        fun fromDao(address: AddressDao) = AddressResponse(
            address.street,
            address.postCode,
            address.city,
            address.country
        )
    }
}
