package application

import persistance.UserLayeredRepository
import java.util.UUID

internal class GetUserQueryHandler {
    private val repository = UserLayeredRepository

    fun handle(id: UUID): UserDto? {
        val user = repository.findById(id) ?: return null

        return UserDto.fromEntity(user)
    }
}
