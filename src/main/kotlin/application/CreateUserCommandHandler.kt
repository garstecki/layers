package application

import domain.Address
import domain.User
import persistance.UserLayeredRepository

internal class CreateUserCommandHandler {
    private val repository = UserLayeredRepository

    fun handle(userDto: UserDto) {
        val user = User(
            userDto.id,
            userDto.name,
            userDto.lastName,
            userDto.birth,
            userDto.email,
            Address(userDto.address.street, userDto.address.postCode, userDto.address.city, userDto.address.country)
        )

        repository.save(user)
    }
}
