package application

import com.neovisionaries.i18n.CountryCode
import domain.Address
import domain.User
import java.util.Date
import java.util.UUID

internal data class UserDto(
    val id: UUID,
    val name: String,
    val lastName: String,
    val birth: Date,
    val email: String,
    val address: AddressDto
) {
    companion object {
        fun fromEntity(user: User) = UserDto(
            user.id,
            user.name,
            user.lastName,
            user.birth,
            user.email,
            AddressDto.fromEntity(user.address)
        )
    }
}

internal data class AddressDto(
    val street: String,
    val postCode: String,
    val city: String,
    val country: CountryCode
) {
    companion object {
        fun fromEntity(address: Address) = AddressDto(address.street, address.postCode, address.city, address.country)
    }
}
