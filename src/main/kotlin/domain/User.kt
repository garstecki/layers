package domain

import com.neovisionaries.i18n.CountryCode
import java.util.Date
import java.util.UUID

internal data class User(
    val id: UUID,
    val name: String,
    val lastName: String,
    val birth: Date,
    val email: String,
    val address: Address
)

internal data class Address(
    val street: String,
    val postCode: String,
    val city: String,
    val country: CountryCode
)
