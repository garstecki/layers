# Layered Architecture benchmark app

Benchmarking app to measure performance differences of app using 4 layers and skipping those layers.
To start testing run tests from `src/test/kotlin/UserTest.kt`. The important tests are:
`execution time of all layers` and `execution time of skipped layers`. Adjust `@ValueSource` values to the number
of records you wish to test.
